#!/usr/bin/env python3
import configparser
import logging
import re
from collections.abc import Awaitable, Iterable
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Literal, NotRequired, TypedDict
from urllib.parse import quote, urlparse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# ASGI Scope
class Scope(TypedDict):
    type: str
    scheme: NotRequired[str]
    server: NotRequired[str]
    path: str
    headers: list[tuple[bytes, bytes]]
    query_string: NotRequired[bytes]


def get_host(scope: Scope) -> str | None:
    headers = scope["headers"]
    for key, value in headers:
        if key == b"host":
            return value.decode("latin-1")

    return None


# ASGI Message
class GenericMessage(TypedDict):
    type: str


class StartMessage(TypedDict):
    type: Literal["http.response.start"]
    status: int
    headers: list[tuple[str, str]]


class BodyMessage(TypedDict):
    type: Literal["http.response.body"]
    body: bytes


Message = GenericMessage | StartMessage | BodyMessage

# ASGI Receive/Send definitions
Receive = Callable[[], Awaitable[Message]]
Send = Callable[[Message], Awaitable[None]]


# ASGI Response helpers

async def respond(
    send: Send, /, headers: dict[str, str], status: int = 200, body: bytes = b""
):
    headers = {
        **headers,
        "Content-Length": str(len(body)),
    }

    await send(
        {
            "type": "http.response.start",
            "status": status,
            "headers": list(headers.items()),
        }
    )
    await send(
        {
            "type": "http.response.body",
            "body": body,
        }
    )


async def plain_response(send: Send, body: str, status: int = 200):
    headers = {"Content-Type": "text/plain"}
    await respond(send, headers=headers, status=status, body=body.encode())


async def redirect(
    send: Send, location: str, status: Literal[307, 308, 301, 302] = 307
):
    headers = {"Location": location}
    await respond(send, headers, status=status)


@dataclass(slots=True)
class Redirection:
    location: str


# Configuration

class AdvancedConfiguration(TypedDict):
    enable_youtube_redirections: bool
    enable_tiktok_redirections: bool


class Config(TypedDict):
    simple_redirections: dict[str, str]
    wildcard_redirections: dict[str, str]
    advanced: AdvancedConfiguration


def build_simple_redirections(d: Iterable[tuple[str, str]]):
    for k, v in d:
        yield k, v
        yield f"www.{k}", v


def get_advanced(
    parser: configparser.ConfigParser, key: str, default: bool = True
) -> bool:
    return bool(parser.get("advanced", key, fallback=default))


def get_config() -> Config:
    redirections_file = Path(__file__).parent / "redirections.ini"
    config_parser = configparser.ConfigParser()
    _ = config_parser.read(redirections_file)

    simple_redirections = build_simple_redirections(
        config_parser["simple_redirections"].items()
    )

    advanced: AdvancedConfiguration = {
        "enable_youtube_redirections": get_advanced(
            config_parser, "enable_youtube_redirections"
        ),
        "enable_tiktok_redirections": get_advanced(
            config_parser, "enable_tiktok_redirections"
        ),
    }

    return {
        "simple_redirections": dict(simple_redirections),
        "wildcard_redirections": dict(config_parser["wildcard_redirections"].items()),
        "advanced": advanced,
    }


config = get_config()


# Request handling

@dataclass(slots=True)
class Request:
    host: str
    url: str
    path: str
    query_string: bytes


yt_q_prog = re.compile(rb"[?&]q=([^&]+)")


def youtube_redirection(req: Request) -> str | None:
    if req.host not in ("www.youtube.com", "youtube.com"):
        return None

    if req.path != "/redirect":
        return None

    q_match = yt_q_prog.search(req.query_string)
    if q_match is not None:
        return q_match.group(1).decode()

    return None


def with_host(url: str, host: str):
    parsed = urlparse(url)
    return parsed._replace(netloc=host).geturl()


def find_redirection(req: Request) -> str | None:
    advanced = config["advanced"]
    if advanced["enable_youtube_redirections"]:
        if redir := youtube_redirection(req):
            return redir

    if advanced["enable_tiktok_redirections"]:
        if req.host == "vm.tiktok.com" and (
            redir_host := config["simple_redirections"].get("tiktok.com")
        ):
            tiktok_url = quote(req.url)
            return f"https://{redir_host}/redirect/search?term{tiktok_url}&type=url"

    if redir_host := config["simple_redirections"].get(req.host):
        return with_host(req.url, redir_host)

    for orig_host, redir_host in config.get("wildcard_redirections", {}).items():
        if req.host == orig_host or req.host.endswith(f".{orig_host}"):
            return with_host(req.url, redir_host)

    return None


async def lifespan_handler(_: Scope, receive: Receive, send: Send) -> None:
    # nothing to do
    message = await receive()
    await send({"type": f"{message['type']}.complete"})


async def app(scope: Scope, receive: Receive, send: Send) -> None:
    if scope["type"] == "lifespan":
        await lifespan_handler(scope, receive, send)
        return

    assert scope["type"] == "http"

    # redirection for masquerade
    host = get_host(scope)
    if host is None:
        await plain_response(send, "unknown host.")
        return

    scheme = scope.get("scheme", "http")
    path = scope["path"]
    query_string = scope.get("query_string", b"")

    masquarade_url = f"{scheme}://{host}{path}"
    masquarade_req = Request(host, masquarade_url, path, query_string)

    redirection = find_redirection(masquarade_req)

    # redirection from path
    if redirection is None:
        pre_url = path[1:]
        # support not passing the scheme, we don’t really use it
        # so we just have to make it parse correctly
        if not pre_url.startswith("http"):
            pre_url = f"https://{pre_url}"

        parsed = urlparse(pre_url)._replace(query=query_string.decode())
        host = parsed.hostname
        assert host is not None
        path = parsed.path

        req = Request(host, parsed.geturl(), path, query_string)
        redirection = find_redirection(req)

    if redirection is None:
        await plain_response(send, f"Unknown domain: {host}", status=404)
    else:
        await redirect(send, redirection)


def gen_hosts(ip: str = "127.0.0.1"):
    for host in config["simple_redirections"]:
        print(ip, host)

    advanced = config["advanced"]
    if advanced["enable_tiktok_redirections"]:
        print(ip, "vm.tiktok.com")


if __name__ == "__main__":
    import sys

    gen_hosts(*sys.argv[1:])
