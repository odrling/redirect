# Redirect

Simple ASGI application/server to redirect to privacy friendly websites.

You can run it with an ASGI web server like uvicorn:

```sh
uvicorn redirect:app
```

running `python redirect.py <hostname>` generates hosts entries to use the server as a 
masquarading server (which doesn’t work for wildcards and specially handled urls and exists more as a “fun” experiment).
